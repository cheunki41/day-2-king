package org.example;


import org.example.model.BarcodeModel;
import org.example.model.ItemModel;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Printer {
    public String print(String[] cart, String[] promotions) {
        List<String> carts = Arrays.stream(cart).collect(Collectors.toList());
        List<String> promotionBarcodes = Arrays.stream(promotions).collect(Collectors.toList());
        List<ItemModel> items = readListOfBarcodes(carts);
        List<ItemModel> promotedItems = getPromotedItems(items, promotionBarcodes);
       return generateReceipt(items, promotedItems);
    }

    private List<ItemModel> readListOfBarcodes(List<String> barcodes) {
        List<BarcodeModel> extractedBarcodes = extractBarcodes(barcodes);
        List<BarcodeModel> groupedBarcodes = groupBarcodes(extractedBarcodes);
        List<ItemModel> items = getBarcodeItems(groupedBarcodes);
        return items;
    }

    public List<BarcodeModel> extractBarcodes(List<String> barcodes) {
        return barcodes.stream().map(barcode -> {
            String[] splits = barcode.split("-");
            if(splits.length == 1) {
                return new BarcodeModel(splits[0], 1);
            }else{
                return new BarcodeModel(splits[0], Float.parseFloat(splits[1]));
            }
        }).collect(Collectors.toList());
    }

    private List<BarcodeModel> groupBarcodes(List<BarcodeModel> barcodeModels) {
        return barcodeModels
                .stream()
                .collect(Collectors.groupingBy(barcodeModel -> barcodeModel.barcode))
                .entrySet()
                .stream()
                .map(entry -> {
                    float totalQuantity = 0;
                    for(BarcodeModel barcodeModel: entry.getValue()) {
                        totalQuantity = totalQuantity + barcodeModel.quantity;
                    }
                    return new BarcodeModel(entry.getKey(), totalQuantity);
                })
                .collect(Collectors.toList());
    }

    private List<ItemModel> getBarcodeItems(List<BarcodeModel> barcodeModels) {
        return barcodeModels.stream().map(barcodeModel -> getBarcodeItem(barcodeModel)).collect(Collectors.toList());
    }

    private ItemModel getBarcodeItem(BarcodeModel barcodeModel) {
        PosDataLoader posDataLoader = new PosDataLoader();
        Map<String, String> items = posDataLoader.loadAllItems();
        String[] metadata = items.get(barcodeModel.barcode).split(",");
        return new ItemModel(barcodeModel.barcode, metadata[0], barcodeModel.quantity, metadata[2], Float.parseFloat(metadata[1]));
    }

    private List<ItemModel> getPromotedItems(List<ItemModel> items, List<String> promotedBarcodes) {
        List<ItemModel> promotedItems = new ArrayList<>();
        items.stream().forEach(itemModel -> {
            if(isInPromotion(itemModel, promotedBarcodes)) {
                Optional<ItemModel> promotedItem = getPromotedItem(itemModel);
                if(promotedItem.isPresent()) {
                    promotedItems.add(promotedItem.get());
                }
            }
        });
        return promotedItems;
    }

    private boolean isInPromotion(ItemModel item, List<String> promotedBarcodes) {
        return promotedBarcodes.contains(item.barcode);
    }

    private Optional<ItemModel> getPromotedItem(ItemModel item) {
        int freeItem = (int) Math.floor(item.quantity / 3);
        if(freeItem == 0) {
            return Optional.empty();
        }
        return Optional.of(new ItemModel(item.barcode, item.name, freeItem, item.unit, item.price));
    }

    private String generateReceipt(List<ItemModel> items, List<ItemModel> promotedItems) {
        List<ItemModel> rankedItems = rankItems(items);
        List<ItemModel> rankedPromotedItems = rankItems(promotedItems);

        String receipt = "***<No Profit Store> Shopping List***\n";
        receipt += "----------------------\n";
        for(ItemModel item: rankedItems) {
            receipt += printItem(item);
        }
        receipt += "----------------------\n";
        if(rankedPromotedItems.size() > 0) {
            receipt += "Buy two get one free items：\n";
            for(ItemModel item: rankedPromotedItems) {
                receipt += printPromotedItems(item);
            }
            receipt += "----------------------\n";
        }
        receipt += String.format("Total：%.2f(CNY)\n", getTotal(items, promotedItems));
        if(rankedPromotedItems.size() > 0) {
            receipt += String.format("Saved：%.2f(CNY)\n", getItemsPrice(promotedItems));
        }

        receipt += "**********************\n";

        return receipt;
    }

    private List<ItemModel> rankItems(List<ItemModel> items) {
        return items.stream().sorted(Comparator.comparing(item -> item.barcode)).collect(Collectors.toList());
    }

    private String printItem(ItemModel itemModel) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        decimalFormat.setDecimalSeparatorAlwaysShown(false);

        String unit = itemModel.unit;
        if(itemModel.quantity > 1) {
            unit += "s";
        }

        return String.format(
                "Name：%s，Quantity：%s %s，Unit Price：%.2f(CNY)，Subtotal：%.2f(CNY)\n",
                itemModel.name,
                decimalFormat.format(itemModel.quantity),
                unit,
                itemModel.price,
                itemModel.price * itemModel.quantity
        );
    }

    private String printPromotedItems(ItemModel itemModel) {
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        decimalFormat.setDecimalSeparatorAlwaysShown(false);

        String unit = itemModel.unit;
        if(itemModel.quantity > 1) {
            unit += "s";
        }

        return String.format(
                "Name：%s，Quantity：%s %s，Value：%.2f(CNY)\n",
                itemModel.name,
                decimalFormat.format(itemModel.quantity),
                unit,
                itemModel.price * itemModel.quantity
        );
    }

    private float getTotal(List<ItemModel> items, List<ItemModel> promotedItems) {
        return getItemsPrice(items) - getItemsPrice(promotedItems);
    }

    private float getItemsPrice(List<ItemModel> items) {
        float price = 0;
        for(ItemModel item: items) {
            price = price + item.price * item.quantity;
        }
        return price;
    }
}

