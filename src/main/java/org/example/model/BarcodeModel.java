package org.example.model;

public class BarcodeModel {
    public String barcode;
    public float quantity;

    public BarcodeModel(String barcode, float quantity) {
        this.barcode = barcode;
        this.quantity = quantity;
    }
}