package org.example.model;

public class ItemModel {
    public String barcode;
    public String name;
    public float quantity;
    public String unit;
    public float price;

    public ItemModel(String barcode, String name, float quantity, String unit, float price) {
        this.barcode = barcode;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.price = price;
    }
}
